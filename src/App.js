import React from 'react';
import './App.css';
import Camera from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isCamOpen: false
    };
    this.handleCamButtonClick = this.handleCamButtonClick.bind(this);
  }

  onCameraStart(stream) {
    // Do stuff on camera start
    console.log('camera started');
  }

  handleCamButtonClick() {
    if (this.state.isCamOpen) {
      this.setState({
        isCamOpen: false
      });
    } else {
      this.setState({
        isCamOpen: true
      });
    }
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
         { this.state.isCamOpen &&
           <Camera onCameraStart = { (stream) => { this.onCameraStart(stream); } } />
         }
         <button onClick={this.handleCamButtonClick}>Camera</button>
        </header>
      </div>
    );
  }
}

export default App;
