# awt_project

# Project Structure

This app is built with:

  - [React JS](https://reactjs.org) on the frontend
  - [Bulma Framework](https://bulma.io/) for scss design

# Setup(for developers)

Make sure you have all the development dependencies installed:

  - A recent NodeJS version
  - The `npm` package manager for JavaScript

Clone the git repo:

```
git clone git@gitlab.com:saqibroy/awt_project.git
```

Then:
  - Run `cd awt_project`
  - Run `npm install` to install all JavaScript packages
  - Run `npm start` to setup and start the development server

If all is fine, you should be able to see the app at `http://localhost:3000`. And on mobile on same network as you machine `http://YOURIP:3000/`.

**NOTE:** please always create a new branch for each task and submit it as a pull request.
